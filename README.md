# [docker-compose](https://github.com/docker/compose) apt packages

### Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-docker-compose.asc https://packaging.gitlab.io/docker-compose/gpg.key
```

### Add repo to apt

```bash
echo "deb [arch=amd64] https://packaging.gitlab.io/docker-compose docker-compose main" | sudo tee /etc/apt/sources.list.d/morph027-docker-compose.list
```
