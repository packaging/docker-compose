#!/bin/bash

set -e

if [[ -n "${DEBUG}" ]]; then
    set -x
fi

export DEBIAN_FRONTEND=noninteractive
tag="${CI_COMMIT_TAG:-v2.15.1+1}"
version="${tag%+*}"
patchlevel="${tag##*+}"
name=docker-compose
description="Define and run multi-container applications with Docker"
architecture="${ARCH:-amd64}"

apt-get -qq update
apt-get -qqy install \
    curl \
    binutils \
    unzip \
    ruby-dev \
    ruby-ffi

type fpm >/dev/null 2>&1 || (
    gem install fpm
)

tmpdir="$(mktemp -d)"

case "${architecture}" in
    "amd64")
        arch="x86_64"
        ;;
    "arm64")
        arch="aarch64"
        ;;
esac

curl -Lo "${tmpdir}/docker-compose" "https://github.com/docker/compose/releases/download/${version}/docker-compose-linux-${arch}"
chmod +x "${tmpdir}/docker-compose"

fpm \
    --input-type dir \
    --output-type deb \
    --force \
    --name "${name}" \
    --package "${CI_PROJECT_DIR}/${name}_${version//v/}+${patchlevel}_${architecture}.deb" \
    --deb-dist docker-compose \
    --architecture "${architecture}" \
    --version "${version//v/}+${patchlevel}" \
    --maintainer "Stefan Heitmüller <stefan.heitmueller@gmx.com>" \
    --url "https://github.com/docker/compose" \
    --description "${description}" \
    --deb-recommends morph027-keyring \
    --prefix "/usr/lib/docker/cli-plugins" \
    --chdir "${tmpdir}" \
    docker-compose
